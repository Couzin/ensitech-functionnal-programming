const isPalindrome = (str, position) => {
  const cleanSpaces = str.replace(" ", "");
  const limit = str.length / 2;
  if (position >= limit) {
    return true;
  }

  let left = position;
  let right = cleanSpaces.length - position - 1;

  if (cleanSpaces[left] !== cleanSpaces[right]) {
    return false;
  }

  return isPalindrome(cleanSpaces, position + 1);
};

console.log(isPalindrome("ni palindrome ne mord ni lapin", 0));