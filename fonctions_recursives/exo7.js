const swap = (arr, i, j) => {
  const temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
};

const permute = (nums) => {
  const result = [];
  
  const backtrack = (current, start) => {
      if (start === current.length - 1) {
          result.push([...current]);
          return;
      }
      
      for (let i = start; i < current.length; i++) {
          swap(current, start, i);
          backtrack(current, start + 1);
          swap(current, start, i);
      }
  };
  
  backtrack(nums, 0);
  return result;
};

const nums = [1, 2, 3];
const permutations = permute(nums);
console.log(permutations);