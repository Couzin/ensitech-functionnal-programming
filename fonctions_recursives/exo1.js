const factorielle = n => {
  if (n === 0) {
      return 1;
  }
  else {
      return n * factorielle(n - 1);
  }
};

console.log(factorielle(5));
console.log(factorielle(0)); 