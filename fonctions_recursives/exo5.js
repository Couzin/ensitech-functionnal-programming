const sumDigitsRecursive = (num) => {
  if (num < 10) {
      return num;
  }
  
  const lastDigit = num % 10;

  const nextDigits = Math.floor(num / 10);
  return lastDigit + sumDigitsRecursive(nextDigits);
};

console.log(sumDigitsRecursive(1234)); // 10