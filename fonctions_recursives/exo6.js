const mergeSort = (arr) => {
  if (arr.length <= 1) {
      return arr;
  }
  
  const mid = Math.floor(arr.length / 2);
  
  const left = arr.slice(0, mid);
  const right = arr.slice(mid);
  
  const sortedLeft = mergeSort(left);
  const sortedRight = mergeSort(right);
  
  return merge(sortedLeft, sortedRight);
};

const merge = (left, right) => {
  let result = [];
  let leftIndex = 0;
  let rightIndex = 0;
  
  while (leftIndex < left.length && rightIndex < right.length) {
      if (left[leftIndex] < right[rightIndex]) {
          result.push(left[leftIndex]);
          leftIndex++;
      } else {
          result.push(right[rightIndex]);
          rightIndex++;
      }
  }
  
  return result.concat(left.slice(leftIndex)).concat(right.slice(rightIndex));
};

const arr = [8, 3, 6, 2, 7, 1, 5, 4, 10, 21 , 12, 11, 9, 13, 14, 15, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 300, -1, -4, -16, 19.9, 21.7];
console.log("Avant le tri fusion :", arr);
const sortedArr = mergeSort(arr);
console.log("Après le tri fusion :", sortedArr);