const myPutStr = (input)  => {
    if (typeof(input) === "number") {
      return "et pourquoi pas 42 ?"
    }
    return false
}

console.log(myPutStr(3))
console.log(myPutStr("toto"))

const computeSurfaceM2 = (longueur, largeur) => {
    return longueur * largeur
}

console.log(computeSurfaceM2(5, 4))
console.log(computeSurfaceM2(2, 2))
