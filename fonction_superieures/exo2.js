const memoize = (fn) => {
  const cache = {};
  return (...args) => {
    const key = JSON.stringify(args);
    if (cache[key] === undefined) {
      cache[key] = fn(...args);
    }
    console.log("cache", cache)
    return cache[key];
  };
};

const slowFunction = (number) => {
  console.log(`Calculating ${number}...`);
  return number * 2;
};

const memoizedFunction = memoize(slowFunction);

console.log(memoizedFunction(3));
console.log(memoizedFunction(4));
console.log(memoizedFunction(30));