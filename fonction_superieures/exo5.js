// but : filtrer nombres pairs, les multiplier par 2 puis les additionner

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const complexOperation = (list) => list
  .filter((num) => num % 2 === 0)
  .map((num) => num * 2)
  .reduce((acc, curr) => acc + curr, 0);

console.log(complexOperation(numbers));