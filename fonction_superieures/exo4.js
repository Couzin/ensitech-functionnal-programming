const add = (f, g) => (x) => f(x) + g(x);
const subtract = (f, g) => (x) => f(x) - g(x);
const multiply = (f, g) => (x) => f(x) * g(x);

const f1 = (x) => x + 1;
const f2 = (x) => x * 2;

const addResult = add(f1, f2);
const subtractResult = subtract(f1, f2);
const multiplyResult = multiply(f1, f2);

console.log("add", addResult(5));
console.log("sub", subtractResult(5));
console.log("mul", multiplyResult(5));
