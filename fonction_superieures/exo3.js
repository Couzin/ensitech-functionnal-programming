const numbers = [1, 2, 3, 4, 5];
const product = (list) => list.reduce((acc, curr) => acc * curr, 1);

console.log(product(numbers));