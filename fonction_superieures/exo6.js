const lazyEvaluation = (calculate) => {
  let cachedValue;
  let isCached = false;

  return () => {
    if (!isCached) {
      cachedValue = calculate();
      isCached = true;
    }
    return cachedValue;
  };
};

const expensiveCalculation = () => {
  console.log("Calculating...");
  return 10 * 20;
};

const lazyCalculation = lazyEvaluation(expensiveCalculation);

console.log("Before calling lazyCalculation");
console.log(lazyCalculation());

// luin e devrait pas afficher calculating
console.log("After calling lazyCalculation");
console.log(lazyCalculation());